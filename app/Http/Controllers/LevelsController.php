<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Level;
use App\Models\Game;
use Validator;

class LevelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'name' => 'required|string|max:255',
            'level' => 'required|integer',
            'game_id' => 'required|integer',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->input('game_id')) {
            $levels = Level::where('game_id','=',$request->input('game_id'))->paginate(10);
        } else {
            $levels = Level::paginate(10);
        }

        $params['levels'] = $levels;
        $params['params'] = ['game_id' => $request->input('game_id')];

        return view('admin.levels.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params['games'] = Game::all();
        $params['game_id'] = $request->input('game_id');
        return view('admin.levels.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('levels/create')->with('error', $validator->messages());
        }

        $level = new Level;
        $level->fill( $request->all() );
        $level->save();

        return redirect('levels')->with('success','Level succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['level'] = Level::find($id);

        if (!$params['level']) {
            return redirect('levels')->with('error','Level no longer exist.');
        }

        // $params['questions'] = Question::where('level_id','=',$id)->paginate(10);

        return view('admin.levels.show',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $params['level'] = Level::find($id);
        $params['games'] = Game::all();

        if (!$params['level']) {
            return redirect('levels')->with('error','Level no longer exist.');
        }

        return view('admin.levels.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $level = Level::find($id);

        if (!$level) {
            return redirect('levels')->with('error','Level no longer exist.');
        }
        
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('levels/'.$id.'/edit')->with('error', $validator->messages());
        }

        $level->fill( $request->all() );
        $level->save();

        return redirect('levels/'.$id.'/edit')->with('success','Level succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = Level::find($id);

        if (!$level) {
            return redirect('levels')->with('error','Level no longer exist.');
        }

        $level->delete();

        return redirect('levels')->with('success','Level successfully removed.');
    }
}
