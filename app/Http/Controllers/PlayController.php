<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Level;
use App\Models\Game;
use App\Models\Logs;
use Auth;

class PlayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $games = Game::all();
        
        return view('dashboard',compact('games'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function level(Request $request, $id)
    {
        $game = Game::find($id);
        $levels = Level::where('game_id','=',$id)->get();

        return view('play.level',compact('levels','game'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function play(Request $request, $level_id, $id)
    {
        $questions = Question::where([
            ['level_id','=',$level_id],
            ['game_id','=',$id],
        ])->inRandomOrder()->get();

        $game = Game::find($id);
        $level = Level::find($level_id);
        $questions = json_encode($questions);

        return view('play.play',compact('questions','level','game'));
    }

    public function score(Request $request,$level_id,$id)
    {
        $pupil_id = $request->session()->get('pupil_id');

        $correct_count = Logs::where([
            ['level_id','=',$level_id],
            ['game_id','=',$id],
            ['user_id','=',$pupil_id],
            ['correct', '=', true]
        ])->count();

        $wrong_count = Logs::where([
            ['level_id','=',$level_id],
            ['game_id','=',$id],
            ['user_id','=',$pupil_id],
            ['correct', '=', false]
        ])->count();

        $question_count = Question::where([
            ['level_id','=',$level_id],
            ['game_id','=',$id]
        ])->count();

        return response()->json([
            'correct_count' => $correct_count,
            'wrong_count' => $wrong_count,
            'question_count' => $question_count
        ]);
    }

    public function answer(Request $request)
    {
        $return = array(
            'error' => true,
            'message' => 'Please try again.'
        );

        $answer = $request->input('answer');
        $question = Question::find($request->input('question_id'));

        if ($question) {

            $answers = json_decode($question->answer);
            $correct = false;

            if (in_array($answer, $answers)) {
                $correct = true;
            }
            
            $pupil_id = $request->session()->get('pupil_id');

            $hasLog = Logs::where([
                ['question_id','=',$question->id],
                ['user_id','=',$pupil_id]
            ])->first();

            $log = ($hasLog) ? $hasLog : new Logs;
            
            $log->answer = $answer;
            $log->correct = $correct;
            $log->question_id = $question->id;
            $log->game_id = $question->game_id;
            $log->level_id = $question->level_id;
            $log->user_id = $pupil_id;
            $log->save();

            $return['question_id'] = $question->id;
            $return['error'] = false;
            $return['message'] = 'Answer recorded.';
        }

        return response()->json($return);
    }
}
