<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Level;
use App\Models\Game;
use Validator;

class GamesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $params['success'] = '';
        $this->validation = [
            'name' => 'required|string|max:255',
            'description' => 'string',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['games'] = Game::paginate(10);
        return view('admin.games.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('games/create')->with('error', $validator->messages());
        }

        $game = new Game;
        $game->fill( $request->all() );
        $game->save();

        return redirect('games')->with('success','Game succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['game'] = Game::find($id);

        if (!$params['game']) {
            return redirect('games')->with('error','Game no longer exist.');
        }

        // $params['questions'] = Question::where('game_id','=',$id)->paginate(10);

        return view('admin.games.show',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $params['game'] = Game::find($id);

        if (!$params['game']) {
            return redirect('games')->with('error','Game no longer exist.');
        }

        return view('admin.games.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $game = Game::find($id);

        if (!$game) {
            return redirect('games')->with('error','Game no longer exist.');
        }
        
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('games/'.$id.'/edit')->with('error', $validator->messages());
        }

        $game->fill( $request->all() );
        $game->save();

        return redirect('games/'.$id.'/edit')->with('success','Game succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $game = Game::find($id);

        if (!$game) {
            return redirect('games')->with('error','Game no longer exist.');
        }

        $game->delete();

        return redirect('games')->with('success','Game successfully removed.');
    }
}
