<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Logs;
use App\Models\Game;
use App\Models\Pupil;
use Auth;
use Image;
use Storage;

class PublicController extends Controller
{
    public function index(){

        if(Auth::check()){

            if (Auth::user()->role == 'user') {
                
                return redirect('play');

            } else {

                return redirect('dashboard');
            }

        } else {
            
            return view('welcome');
        }
    }

    public function play(Request $request)
    {
        $games = Game::all();

        $images = [
            '',
            '/img/color-game.png',
            '/img/number-game.png',
            '/img/sound-game.png',
            '/img/alphabet-game.png'
        ];

        $pupilID = $request->session()->get('pupil_id');

        $logs = Logs::where('user_id','=',$pupilID)->paginate(10);
        $pupil = Pupil::find($pupilID);

        if(!$pupil){
            $request->session()->forget('pupil_id');
            return redirect('login');
        }

        return view('play.start',compact('games','images','pupil','logs'));
    }

    public function img($filename)
    {
        // dd(Storage::get('public/'.$filename));
        return Image::make(Storage::get('public/'.$filename))->response();
    }

    public function pupil_login(Request $request)
    {
        $username = Str::slug($request->input('username'), '_');
        
        $pupil = Pupil::where('username','=',$username)->first();

        if ($pupil) {
            $request->session()->put('pupil_id', $pupil->id);
            return redirect('play');
        }

        return redirect('login')->with('error','Username not found, please try again.');
    }
}
