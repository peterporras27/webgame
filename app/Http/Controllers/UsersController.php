<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params['users'] = User::paginate(10);

        return view('admin.users.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('users/create')->withInput($request->all())->with('error', $validator->messages());
        }

        $user = new User;
        $user->fill( $request->all() );
        // $user->pwd = $request->input('password');
        $user->password = Hash::make( $request->input('password') );   
        $user->save();

        return redirect('users')->with('success', 'Admin successfully registered.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return redirect('users')->with('error','Admin no longer exist.');        
        }
        
        $params['user'] = User::find($id);
        return view('admin.users.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validation = $this->validation;

        // check if password is changed.
        if ( empty($request->input('password')) ) {
            unset($validation['password']);
        }

        if ($request->input('email')==$user->email) {
            $validation['email'] = 'required|email|max:255';
        }

        if ( $request->input('username')==$user->username ) {
            $validation['username'] = 'required|string|max:255';
        }

        // Input validation
        $validator = Validator::make( $request->all(), $validation );

        if( $validator->fails() ) 
        {
            return redirect('users/'.$id.'/edit')->with('error', $validator->messages());
        }

        $user->username = $request->input('username');
        // $user->first_name = $request->input('first_name');
        // $user->last_name = $request->input('last_name');
        // $user->middle_name = $request->input('middle_name');
        $user->email = $request->input('email');
        $user->role = 'admin';

        if ( !empty($request->input('password')) ) {
            $user->password = Hash::make($request->input('password'));   
            // $user->pwd = $request->input('password');
        }

        $user->save();

        return redirect('users/'.$id.'/edit')->with('success', 'Admin details successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $q = User::find($id);

        if (!$q) 
        {
            return redirect('users')->with('error','Admin no longer exists!');
        }

        $q->delete();

        return redirect('users')->with('success','Admin successfully removed.');
    }
}
