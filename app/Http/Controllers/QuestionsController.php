<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Level;
use App\Models\Game;
use Validator;
use Storage;

class QuestionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'question' => 'required|array|max:255',
            'answer' => 'required|array',
            'level_id' => 'required|integer',
            'game_id' => 'required|integer'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->input('game_id')) {
            $questions = Question::where('game_id','=',$request->input('game_id'))->paginate(10);
        } else {
            $questions = Question::paginate(10);
        }

        $params['questions'] = $questions;
        $params['params'] = ['game_id' => $request->input('game_id')];
        return view('admin.questions.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *  
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params['games'] = Game::all();
        
        $levels = Level::orderBy('id','desc')->get();
        $levelsArr = [];

        foreach ($levels as $level) {
            $levelsArr[$level->game_id][] = [
                'id' => $level->id,
                'level' => $level->level,
                'name' => $level->name
            ];
        }

        $params['levels'] = $levelsArr;
        $params['game_id'] = $request->input('game_id');

        return view('admin.questions.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('questions/create')->with('errors', $validator->messages());
        }

        $allowedfileExtension = ['jpg','png','jpeg','gif','wav','mp3'];

        $question = new Question;
        // $question->question = $request->input('question');
        $question->answer = json_encode($request->input('answer'));
        $question->game_id = $request->input('game_id');
        $question->level_id = $request->input('level_id');

        $q = $request->input('question');
        $a = $request->input('a');
        $b = $request->input('b');
        $c = $request->input('c');
        $d = $request->input('d');

        if($request->hasFile('question'))
        {
            $file_q = $request->file('question');

            // $filename = $file_q['image']->getClientOriginalName();
            $extension = $file_q['image']->getClientOriginalExtension();
            $check = in_array($extension,$allowedfileExtension);

            if($check)
            {
                $q['image'] = str_replace('public/', '', $request->question['image']->store('public'));
            }
        }

        if($request->hasFile('a'))
        {
            $file_a = $request->file('a');

            // $filename = $file_a['image']->getClientOriginalName();
            $extension = $file_a['image']->getClientOriginalExtension();
            $check = in_array($extension,$allowedfileExtension);

            if($check)
            {
                $a['image'] = str_replace('public/', '', $request->a['image']->store('public'));
            }
        }

        if($request->hasFile('b'))
        {
            $file_b = $request->file('b');

            // $filename = $file_b['image']->getClientOriginalName();
            $extension = $file_b['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $b['image'] = str_replace('public/', '', $request->b['image']->store('public'));
            }
        }

        if($request->hasFile('c'))
        {
            $file_c = $request->file('c');

            // $filename = $file_c['image']->getClientOriginalName();
            $extension = $file_c['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $c['image'] = str_replace('public/', '', $request->c['image']->store('public'));
            }
        }

        if($request->hasFile('d'))
        {
            $file_d = $request->file('d');

            // $filename = $file_d['image']->getClientOriginalName();
            $extension = $file_d['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $d['image'] = str_replace('public/', '', $request->d['image']->store('public'));
            }
        }

        $question->question = json_encode($q);
        $question->a = json_encode($a);
        $question->b = json_encode($b);
        $question->c = json_encode($c);
        $question->d = json_encode($d);

        $question->save();

        return redirect('questions')->with('success','Question succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['question'] = Level::find($id);

        if (!$params['question']) {
            return redirect('questions')->with('errors','Question no longer exist.');
        }

        // $params['questions'] = Question::where('question_id','=',$id)->paginate(10);

        return view('admin.questions.show',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        
        if (!$question) {
            return redirect('questions')->with('errors','Question no longer exist.');
        }

        $levels = Level::orderBy('id','desc')->get();
        $params['games'] = Game::all();
        $params['question'] = $question;

        $levelsArr = [];

        foreach ($levels as $level) {
            $levelsArr[$level->game_id][] = [
                'id' => $level->id,
                'level' => $level->level,
                'name' => $level->name
            ];
        }

        $params['levels'] = $levelsArr;

        $params['q'] = json_decode($question->question);
        $params['ans'] = json_decode($question->answer);
        $params['a'] = json_decode($question->a);
        $params['b'] = json_decode($question->b);
        $params['c'] = json_decode($question->c);
        $params['d'] = json_decode($question->d);

        return view('admin.questions.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('questions/'.$id.'/edit')->with('error', $validator->messages());
        }

        $allowedfileExtension = ['jpg','png','jpeg','gif','wav','mp3'];

        $question = Question::find($id);

        if (!$question) {
            return redirect('questions')->with('errors', 'Question no longer exist.');
        }

        // $question->question = $request->input('question');
        $question->answer = json_encode($request->input('answer'));
        $question->game_id = $request->input('game_id');
        $question->level_id = $request->input('level_id');

        $q = $request->input('question');
        $a = $request->input('a');
        $b = $request->input('b');
        $c = $request->input('c');
        $d = $request->input('d');

        $img_q = json_decode( $question->question );

        if($request->hasFile('question'))
        {
            $file_q = $request->file('question');

            // check if file is valid.
            if(in_array($file_q['image']->getClientOriginalExtension(),$allowedfileExtension))
            {
                // save file
                $q['image'] = str_replace('public/', '', $request->question['image']->store('public'));

                // delete old image if exist.
                if ( isset($img_q->image) ) {
                    if (Storage::exists('public/'.$img_q->image)) {
                        Storage::delete('public/'.$img_q->image);   
                    }
                }

            } else {
                
                $q['image'] = ( isset($img_q->image) ) ? $img_q->image:'';
            }

        } else {

            $q['image'] = ( isset($img_q->image) ) ? $img_q->image:'';
        }

        $img_a = json_decode( $question->a );

        // check if input has file
        if($request->hasFile('a'))
        {
            $file_a = $request->file('a');
            // $filename = $file_a['image']->getClientOriginalName();
            $extension = strtolower($file_a['image']->getClientOriginalExtension());

            // check if file is valid.
            if(in_array($extension,$allowedfileExtension))
            {
                // save file
                $a['image'] = str_replace('public/', '', $request->a['image']->store('public'));

                // delete old image if exist.
                if ( isset($img_a->image) ) {
                    if (Storage::exists('public/'.$img_a->image)) {
                        Storage::delete('public/'.$img_a->image);   
                    }
                }

            } else {
                
                $a['image'] = ( isset($img_a->image) ) ? $img_a->image:'';
            }

        } else {

            $a['image'] = ( isset($img_a->image) ) ? $img_a->image:'';            
        }

        $img_b = json_decode( $question->b );

        if($request->hasFile('b'))
        {
            $file_b = $request->file('b');
            // $filename = $file_b['image']->getClientOriginalName();
            $extension = strtolower($file_b['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $b['image'] = str_replace('public/', '', $request->b['image']->store('public'));

                if ( isset($img_b->image) ) {
                    if (Storage::exists('public/'.$img_b->image)) {
                        Storage::delete('public/'.$img_b->image);      
                    } 
                }

            } else {
                
                $b['image'] = ( isset($img_b->image) ) ? $img_b->image:'';
            }

        } else {

            $b['image'] = ( isset($img_b->image) ) ? $img_b->image:'';
        }

        $img_c = json_decode( $question->c );

        if($request->hasFile('c'))
        {
            $file_c = $request->file('c');

            // $filename = $file_c['image']->getClientOriginalName();
            $extension = strtolower($file_c['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $c['image'] = str_replace('public/', '', $request->c['image']->store('public'));
                
                if ( isset($img_c->image) ) {
                    if (Storage::exists('public/'.$img_c->image)) {
                        Storage::delete('public/'.$img_c->image);      
                    }
                }

            } else {
                
                $c['image'] = ( isset($img_c->image) ) ? $img_c->image:'';
            }

        } else {

            $c['image'] = ( isset($img_c->image) ) ? $img_c->image:'';            
        }

        $img_d = json_decode( $question->d );

        if($request->hasFile('d'))
        {
            $file_d = $request->file('d');

            // $filename = $file_d['image']->getClientOriginalName();
            $extension = strtolower($file_d['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $d['image'] = str_replace('public/', '', $request->d['image']->store('public'));

                if ( isset($img_d->image) ) {
                    if (Storage::exists('public/'.$img_d->image)) {
                        Storage::delete('public/'.$img_d->image);   
                    }
                }

            } else {
                
                $d['image'] = ( isset($img_d->image) ) ? $img_d->image:'';
            }

        }  else {
        
            $d['image'] = ( isset($img_d->image) ) ? $img_d->image:'';            
        }

        $question->question = json_encode($q);
        $question->a = json_encode($a);
        $question->b = json_encode($b);
        $question->c = json_encode($c);
        $question->d = json_encode($d);

        $question->save();

        return redirect('questions/'.$id.'/edit')->with('success','Question succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);

        if (!$question) {
            return redirect('questions')->with('errors','Question no longer exist.');
        }

        $img_q = json_decode( $question->question );
        $img_a = json_decode( $question->a );
        $img_b = json_decode( $question->b );
        $img_c = json_decode( $question->c );
        $img_d = json_decode( $question->d );
        if ( isset($img_q->image) ) {if (Storage::exists('public/'.$img_q->image)) {Storage::delete('public/'.$img_q->image);}}
        if ( isset($img_a->image) ) {if (Storage::exists('public/'.$img_a->image)) {Storage::delete('public/'.$img_a->image);}}
        if ( isset($img_b->image) ) {if (Storage::exists('public/'.$img_b->image)) {Storage::delete('public/'.$img_b->image);}}
        if ( isset($img_c->image) ) {if (Storage::exists('public/'.$img_c->image)) {Storage::delete('public/'.$img_c->image);}}
        if ( isset($img_d->image) ) {if (Storage::exists('public/'.$img_d->image)) {Storage::delete('public/'.$img_d->image);}}

        $question->delete();

        return redirect('questions')->with('success','Question successfully removed.');
    }
}
