<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Pupil;
use App\Models\User;
use App\Models\Logs;
use Validator;

class PupilsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'username' => 'required|string|max:255|unique:pupils'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['pupils'] = Pupil::paginate(20);
        return view('admin.pupils.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pupils.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['username'] = Str::slug($request->input('username'), '_');

        // Input validation
        $validator = Validator::make( $data, $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('pupils/create')->withInput($request->all())->with('error', $validator->messages());
        }

        $user = new Pupil;
        $user->fill( $data );
        $user->save();

        return redirect('pupils')->with('success', 'Pupil successfully registered.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $logs = Logs::where('user_id','=',$id)->paginate(10);
        $pupil = Pupil::find($id);

        return view('admin.pupils.show',compact('logs','pupil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pupil = Pupil::find($id);

        if (!$pupil) {
            return redirect('pupils')->with('error','Pupil no longer exist.');        
        }
        
        $params['pupil'] = Pupil::find($id);
        return view('admin.pupils.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pupil = Pupil::find($id);

        $validation = $this->validation;

        if ( $request->input('username') == $pupil->username ) {
            $validation['username'] = 'required|string|max:255';
        }

        // Input validation
        $validator = Validator::make( $request->all(), $validation );

        if( $validator->fails() ) 
        {
            return redirect('pupils/'.$id.'/edit')->with('error', $validator->messages());
        }

        $username = Str::slug($request->input('username'), '_');

        $pupil->username = $username;
        $pupil->save();

        return redirect('pupils/'.$id.'/edit')->with('success', 'Pupil details successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $q = Pupil::find($id);

        if (!$q) {
            return redirect('users')->with('error','Pupil no longer exists!');
        }

        $q->delete();

        $logs = Logs::where('user_id','=',$id)->get();

        if ($logs->count()) {
            foreach ($logs as $log) {
                $log->delete();
            }
        }

        return redirect('users')->with('success','Pupil successfully removed.');
    }
}
