<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'a', 'b', 'c', 'd', 'answer', 'level_id', 'game_id'
    ];

    public function category(){
        return $this->hasOne(Game::class, 'id', 'game_id');
    }

    public function level(){
        return $this->hasOne(Level::class, 'id', 'level_id');
    }
}
