<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pupil extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'username',
        'points'
    ];

    public function score(){
        return Logs::where([
            ['user_id', '=', $this->id],
            ['correct', '=', true]
        ])->count();
    }
}
