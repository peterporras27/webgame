<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    use HasFactory;

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function question(){
        return $this->hasOne(Question::class, 'id', 'question_id');
    }

    public function game(){
        return $this->hasOne(Game::class, 'id', 'game_id');
    }

    public function level(){
        return $this->hasOne(Level::class, 'id', 'level_id');
    }
}
