<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ ucwords(str_replace('_', ' ', $pupil->username)) }}'s score
            <a href="{{ route('scores') }}" class="btn btn-sm btn-dark float-right">Go back</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6">
                    
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <x-auth-session-status class="mb-4" :status="session('success')" />
                    
                    <div class="card">
                        <div class="card-header">
                            <b>{{ ucwords(str_replace('_', ' ', $pupil->username)) }}'s score</b>
                        </div>
                        <div class="card-body">
                            @if($logs->count())
                            
                            <table class="table table-responsive table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Answer</th>
                                        <th>Game / Level</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($logs as $log)
                                        <?php $q = json_decode($log->question->question); ?>
                                        <tr>
                                            <td>{{ $q->text }}</td>
                                            <td>{{ strtoupper($log->answer) }}</td>
                                            <td>{{ $log->game->name . ' / '. $log->level->name}}</td>
                                            <td>
                                                @if($log->correct)
                                                <span class="badge badge-pill badge-success">Correct</span>
                                                @else
                                                <span class="badge badge-pill badge-danger">Wrong</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>

                            {{ $logs->links() }}

                        @else

                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Oops!</strong> There are no scores to show at the moment.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                        @endif
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
