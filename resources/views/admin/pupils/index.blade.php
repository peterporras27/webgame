<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pupils') }} <a href="{{ route('pupils.create') }}" class="btn btn-primary btn-sm float-right">Register Pupil</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    @if($pupils->count())
                        
                        <table class="table table-responsive table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Points</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($pupils as $pupil)
                                    
                                    <tr>
                                        <td>{{ ucwords(str_replace('_', ' ', $pupil->username)) }}</td>
                                        <td>{{ $pupil->score() }}</td>
                                        <td>
                                            <a href="#" onclick="removeItem({{$pupil->id}})" class="btn btn-sm btn-danger float-right">Remove <i class="bi bi-x-lg"></i></a>
                                            <a href="{{ route('pupils.edit',$pupil->id) }}" class="btn btn-sm btn-info float-right mr-4">Edit <i class="bi bi-pencil-square"></i></a>
                                            <form id="delete-{{$pupil->id}}" action="{{ route('pupils.destroy',$pupil->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                    
                                @endforeach
                            </tbody>
                        </table>

                        {{ $pupils->links() }}
                    @else 
                        <div class="alert alert-warning" role="alert">
                          <h4 class="alert-heading">Oops!</h4>
                          <p>No pupils to show at the moment.</p>
                          <hr>
                          <br>
                          <a href="{{ route('pupils.create') }}" class="btn btn-dark btn-sm">Register Pupil</a>
                        </div>
                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <script>
        function removeItem(id) {
            var txt;
            if (confirm("Are you sure?")) {
                jQuery('#delete-'+id).submit();
            }
        }
    </script>
</x-app-layout>
