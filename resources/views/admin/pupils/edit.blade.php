<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Pupil') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <x-auth-session-status class="mb-4" :status="session('success')" />
                    
                    <div class="card">
                        <div class="card-header">
                            Edit Pupil
                        </div>
                        <div class="card-body">
                            <form action="{{ route('pupils.update', $pupil->id) }}" method="POST" role="form">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exam-username">Username:</label>
                                            <input type="text" class="form-control" name="username" value="{{ ucwords(str_replace('_', ' ', $pupil->username)) }}" required>
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <br>
                                <button type="submit" class="btn btn-primary">Save &nbsp;<i class="bi bi-save"></i></button>
                            </form>
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
