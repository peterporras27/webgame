<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Questions') }} <a href="{{ route('questions.create', $params) }}" class="btn btn-primary btn-sm float-right">Create a question</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    @if($questions->count())
                        
                        <table class="table table-responsive table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Category</th>
                                    <th>Level</th>
                                    <th class="text-right">Options</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($questions as $question)
                                    <?php $q = json_decode( $question->question );?>
                                    <tr>
                                        <td>{{ $q->text }}</td>
                                        <td>{{ $question->category->name }}</td>
                                        <td>{{ $question->level->name }}</td>
                                        <td>
                                            <a href="#" onclick="removeItem({{$question->id}})" class="btn btn-sm btn-danger float-right">Delete</a>
                                            <a href="{{ route('questions.edit',$question->id) }}" class="btn btn-sm btn-info float-right mr-4">Edit</a>
                                            <form id="delete-{{$question->id}}" action="{{ route('questions.destroy',$question->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                        {{ $questions->links() }}
                    @else 
                        <div class="alert alert-info" role="alert">
                            Oops! there are no available records to show at the moment.
                        </div>
                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <script>
        function removeItem(id) {
            var txt;
            if (confirm("Are you sure?")) {
                jQuery('#delete-'+id).submit();
            }
        }
    </script>
</x-app-layout>
