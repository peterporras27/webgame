<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Question') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form action="{{ route('questions.store') }}" method="POST" role="form" enctype="multipart/form-data">
                        @csrf

                        <div class="card bg-light mb-3">
                            <div class="card-header">Question</div>
                            <div class="card-body">
                                

                                <div class="row">
                                    <div class="col">
                                        <label>Question:</label>
                                        <div class="form-group">
                                            <textarea type="text" name="question[text]" class="form-control" placeholder="What is your question?"></textarea>
                                        </div>
                                        <label>Game:</label>
                                        <div class="form-group">
                                            <select name="game_id" id="" class="form-control">
                                                @foreach($games as $game)
                                                    <option value="{{ $game->id }}"{{ $game_id == $game->id ? ' selected':'' }}>{{ $game->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label>Level:</label>
                                        <div class="form-group">
                                            <select name="level_id" id="" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="preview-question"></div>
                                        <br>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="question[image]" class="custom-file-input" data-id="question" id="imgupload-question">
                                                <label class="custom-file-label" for="imgupload-question">Choose image / audio</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">

                                <div class="card bg-light mb-3">
                                    <div class="card-header">A</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="checkbox" name="answer[]" value="a" class="form-control js-switch" checked> 
                                            <span class="avail badge badge-success">Correct Answer</span>
                                        </div> 
                                        <div class="form-group">
                                            <input type="text" name="a[text]" class="form-control" placeholder="Answer">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="a[image]" class="custom-file-input" data-id="a" id="imgupload-a">
                                                <label class="custom-file-label" for="imgupload-a">Choose image / audio</label>
                                            </div>
                                        </div>
                                        <div class="preview-a"></div>
                                    </div>
                                </div>

                                <div class="card bg-light mb-3 level-2">
                                    <div class="card-header">C</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="checkbox" name="answer[]" value="c" class="form-control js-switch"> 
                                            <span class="avail badge badge-warning">Wrong Answer</span>
                                        </div> 
                                        <div class="form-group">
                                            <input type="text" name="c[text]" class="form-control" placeholder="Answer">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="c[image]" class="custom-file-input" data-id="c" id="imgupload-c">
                                                <label class="custom-file-label" for="imgupload-a">Choose image / audio</label>
                                            </div>
                                        </div>
                                        <div class="preview-c"></div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col">

                                <div class="card bg-light mb-3">
                                    <div class="card-header">B</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="checkbox" name="answer[]" value="b" class="form-control js-switch"> 
                                            <span class="avail badge badge-warning">Wrong Answer</span>
                                        </div> 
                                        <div class="form-group">
                                            <input type="text" name="b[text]" class="form-control" placeholder="Answer">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="b[image]" class="custom-file-input" data-id="b" id="imgupload-b">
                                                <label class="custom-file-label" for="imgupload-a">Choose image / audio</label>
                                            </div>
                                        </div>
                                        <div class="preview-b"></div>
                                    </div>
                                </div>

                                <div class="card bg-light mb-3 level-3">
                                    <div class="card-header">D</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="checkbox" name="answer[]" value="d" class="form-control js-switch"> 
                                            <span class="avail badge badge-warning">Wrong Answer</span>
                                        </div> 
                                        <div class="form-group">
                                            <input type="text" name="d[text]" class="form-control" placeholder="Answer">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" name="d[image]" class="custom-file-input" data-id="d" id="imgupload-d">
                                                <label class="custom-file-label" for="imgupload-a">Choose image / audio</label>
                                            </div>
                                        </div>
                                        <div class="preview-d"></div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save &nbsp;<i class="fa fa-save"></i></button>
                    </form>

                </div>
            </div>
        </div>
    </div>
<script>
var $=jQuery;

var levels = {!! json_encode($levels) !!};
// Multiple images preview in browser
var imagesPreview = function(input, placeToInsertImagePreview) 
{
    if (input.files) 
    {
        // console.log(input.files);
        var filesAmount = input.files.length;
        var validFiles = ['audio/mp3','audio/wav'];

        for (i = 0; i < filesAmount; i++) 
        {
            if (validFiles.includes(input.files[i].type)) {
                
                var soundurl = URL.createObjectURL(input.files[i]);
                var soundhtml = '<figure><audio controls src="'+soundurl+'">Your browser does not support the</audio></figure>';
                $($.parseHTML(soundhtml)).appendTo(placeToInsertImagePreview);

            } else {

                var reader = new FileReader();
                reader.onload = function(event) {
                    var r = '<img class="img-fluid img-thumbnail" src="'+event.target.result+'">';
                    $($.parseHTML(r)).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    }
};

$('.custom-file-input').on('change', function() {
    var i = $(this).data('id');
    $('.preview-'+i).html('');
    imagesPreview(this, 'div.preview-'+i);
});

$(document).ready(function(){

    var gameid = $('[name="game_id"]').val();
    popUlate(levels[gameid]);

    $('[name="game_id"]').change(function(event) {
        var gameid = $(this).val();
        popUlate(levels[gameid]);
    });

    $('[name="level_id"]').change(function(event) {
        setLevel();
    });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new Switchery(html);
    });

    $('.js-switch').change(function(){

        if ($(this).prop('checked')) {

            $(this).parent().find('.avail').text('Correct Answer');
            $(this).parent().find('.avail').removeClass('badge-warning');
            $(this).parent().find('.avail').addClass('badge-success');

        } else {

            $(this).parent().find('.avail').text('Wrong Answer');
            $(this).parent().find('.avail').removeClass('badge-success');
            $(this).parent().find('.avail').addClass('badge-warning');
        }
    });
});

function setLevel()
{
    var level = $('[name="level_id"]').find(':selected').data('level');
    
    switch(level){
        case 1: 
            $('.level-2, .level-3').hide();
            break;
        case 2: 
            $('.level-2').fadeIn();
            $('.level-3').hide();
            break;
        case 3: 
            $('.level-2').fadeIn();
            $('.level-3').fadeIn();
            break;
        default:
            $('.level-2, .level-3').hide();
            break;
    }
}

function popUlate(obj){

    var html = '';
    for (var i = obj.length - 1; i >= 0; i--) {
        html += '<option value="'+obj[i].id+'" data-level="'+obj[i].level+'">'+obj[i].name+'</option>';
    }

    $('[name="level_id"]').html(html);
    setLevel();
}
</script>
</x-app-layout>
