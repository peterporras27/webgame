<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Levels') }} <a href="{{ route('levels.create', $params) }}" class="btn btn-primary btn-sm float-right">Create a level</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    @if($levels->count())
                        <?php
                        //Columns must be a factor of 12 (1,2,3,4,6,12)
                        $numOfCols = 4;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;
                        ?>
                        <div class="row">
                        @foreach($levels as $level)

                            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                                <div class="card bg-light border-light mb-4" style="max-width: 18rem;">
                                    <div class="card-header">
                                        <b>{{ $level->name }}</b>
                                    </div>
                                    <div class="card-body text-dark">
                                        <table class="table table-sm table-bordered mb-0">
                                            <tbody>
                                                <tr>
                                                    <td><b>Game:</b></td>
                                                    <td>{{ $level->game->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Level:</b></td>
                                                    <td>{{ $level->level }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col">
                                                <a href="#" onclick="removeItem({{$level->id}})" class="btn btn-sm btn-danger btn-block">Delete</a>
                                            </div>
                                            <div class="col">
                                                <a href="{{ route('levels.edit',$level->id) }}" class="btn btn-sm btn-info btn-block">Edit</a>
                                            </div>
                                        </div>
                                        <form id="delete-{{$level->id}}" action="{{ route('levels.destroy',$level->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <?php
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                            ?>
                        @endforeach
                        </div>

                        {{ $levels->links() }}
                    @else 

                    <div class="alert alert-info" role="alert">
                        Oops! there are no available records to show at the moment.
                    </div>

                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <script>
        function removeItem(id) {
            var txt;
            if (confirm("Are you sure?")) {
                jQuery('#delete-'+id).submit();
            }
        }
    </script>
</x-app-layout>
