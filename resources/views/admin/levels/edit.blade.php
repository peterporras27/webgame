<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Level') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    <form action="{{ route('levels.update', $level->id) }}" method="POST" role="form">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="exam-name">Level Name:</label>
                            <input type="text" class="form-control" name="name" value="{{ $level->name }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exam-name">Game:</label>
                                    <select class="form-control" name="game_id" required>
                                        @foreach($games as $game)
                                        <option value="{{ $game->id }}"{{ $level->game_id == $game->id ? ' selected':'' }}>{{ $game->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exam-name">Level:</label>
                                    <select class="form-control" name="level" required>
                                        <option value="1"{{ $level->level == '1' ? ' selected':'' }}>Level 1</option>
                                        <option value="2"{{ $level->level == '2' ? ' selected':'' }}>Level 2</option>
                                        <option value="3"{{ $level->level == '3' ? ' selected':'' }}>Level 3</option>
                                    </select>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        <br>
                        <button type="submit" class="btn btn-primary btn-lg">Update &nbsp;<i class="fa fa-save"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
