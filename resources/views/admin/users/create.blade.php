<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Register Admin') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <div class="card">
                        <div class="card-header">
                            Register Admin
                        </div>
                        <div class="card-body">
                            <form action="{{ route('users.store') }}" method="POST" role="form">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exam-name">Username:</label>
                                            <input type="text" class="form-control" name="username" required>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exam-email">Email:</label>
                                            <input type="email" class="form-control" name="email" required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exam-password">Password:</label>
                                            <input type="password" class="form-control" name="password" required>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exam-password_confirmation">Repeat Password:</label>
                                            <input type="password" class="form-control" name="password_confirmation" required>
                                            @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <br>
                                <button type="submit" class="btn btn-primary">Register &nbsp;<i class="bi bi-save"></i></button>
                            </form>
                        </div>
                    </div>

                    

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
