<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Admins') }} <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm float-right">Register Admin</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    @if($users->count())
                        
                        <table class="table table-responsive table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Email</th>
                                    {{-- <th>Points</th> --}}
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($users as $user)
                                    @if($user->role == 'admin')
                                    <tr>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        {{-- <td>{{ $user->points }}</td> --}}
                                        <td>
                                            <a href="#" onclick="removeItem({{$user->id}})" class="btn btn-sm btn-danger float-right">Remove <i class="bi bi-x-lg"></i></a>
                                            <a href="{{ route('users.edit',$user->id) }}" class="btn btn-sm btn-info float-right mr-4">Edit <i class="bi bi-pencil-square"></i></a>
                                            <form id="delete-{{$user->id}}" action="{{ route('users.destroy',$user->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>

                        {{ $users->links() }}
                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <script>
        function removeItem(id) {
            var txt;
            if (confirm("Are you sure?")) {
                jQuery('#delete-'+id).submit();
            }
        }
    </script>
</x-app-layout>
