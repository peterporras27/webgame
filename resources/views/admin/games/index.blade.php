<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Games') }} <a href="{{ route('games.create') }}" class="btn btn-primary btn-sm float-right">Create a game</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    @if($games->count())
                        <?php
                        //Columns must be a factor of 12 (1,2,3,4,6,12)
                        $numOfCols = 3;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;
                        ?>
                        <div class="row">
                        @foreach($games as $game)

                            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                                <div class="card bg-light border-light mb-4">
                                    <div class="card-header">
                                        <b>{{ $game->name }}</b>
                                        <a href="#" onclick="removeItem({{$game->id}})" class="btn btn-sm btn-danger float-right"><i class="bi bi-x-circle-fill"></i></a>
                                    </div>
                                    <div class="card-body text-dark">
                                        <p class="card-text">{{ $game->description }}</p>
                                    </div>
                                    <div class="card-footer" align="center">
                                        <a href="{{ route('games.edit',$game->id) }}" class="btn btn-sm btn-success">Edit <i class="bi bi-pencil-square"></i></a>
                                        <a href="{{ route('levels.index',['game_id'=> $game->id]) }}" class="btn btn-sm btn-info">Levels <i class="bi bi-arrow-up-right-square-fill"></i></a>
                                        <a href="{{ route('questions.index',['game_id'=> $game->id]) }}" class="btn btn-sm btn-primary">Questions <i class="bi bi-patch-question-fill"></i></a>
                                        <form id="delete-{{$game->id}}" action="{{ route('games.destroy',$game->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                            ?>
                        @endforeach
                        </div>
                        {{ $games->links() }}
                    @else 

                        <div class="alert alert-info" role="alert">
                            Oops! there are no available records to show at the moment.
                        </div>
                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <script>
        function removeItem(id) {
            var txt;
            if (confirm("Are you sure?")) {
                jQuery('#delete-'+id).submit();
            }
        }
    </script>
</x-app-layout>
