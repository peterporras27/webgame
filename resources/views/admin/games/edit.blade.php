<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Game') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <x-auth-session-status class="mb-4" :status="session('success')" />

                    <form action="{{ route('games.update', $game->id) }}" method="POST" role="form">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="exam-name">Name:</label>
                            <input type="text" class="form-control" name="name" value="{{ $game->name }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                   
                        <div class="form-group">
                            <label for="exam-duration">Decription:</label>
                            <textarea class="form-control" name="description" required>{{ $game->description }}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        <hr>
                        <br>
                        <button type="submit" class="btn btn-primary btn-lg">Update &nbsp;<i class="fa fa-save"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
