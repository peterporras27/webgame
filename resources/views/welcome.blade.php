<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            {{-- <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a> --}}
        </x-slot>

        <!-- Remember Me -->
        <div class="block mt-3" align="center">

            @if (Route::has('login'))

                <h1 style="font-size: 3rem;"><b>ONLINE LEARNING GAMES</b></h1>
                <br>
                <button 
                    style="padding: 20px;font-size: 4rem;" 
                    onclick="window.location.href='{{ route('login') }}';" 
                    class="btn-block btn-success btn-lg btn">
                    <b>{{ __('LOG IN') }}</b>
                </button>

            @endif
        </div>

    </x-auth-card>
</x-guest-layout>
