<x-guest-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-2 mb-5">
    <style>
        .answers button{
            width: 100%;
            height: 150px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
            border: 5px solid #ccc;
            border-radius: 5px;
        }
        .answers button:hover{
            border: 5px solid #c3e6cb;
            background-color: #d4edda;
        }

        .answers button span{
            display: none;
            background-color: #29a745;
            border-radius: 10px;
            padding: 5px 10px;
            position: absolute;
            top: -15px;
            right: 0px;
            color: #fff;
            font-weight: 900;
        }

        .selected{
            border: 5px solid #c3e6cb !important;
            background-color: #d4edda;
        }

        @media only screen and (max-width: 767px) {
            .question img {
                width:  50%;
            }
        }
    </style>
    
    <div class="card text-center">
        <div class="card-header">
            <b>{{ $game->description }}</b>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col"></div>
                <div class="col-md-8">
                    <div class="question" align="center">
                        <img src="https://via.placeholder.com/300" alt="">
                        <h3></h3>
                    </div>
                </div>
                <div class="col"></div>
            </div>
            <br><br>
            <div class="row">
                <div class="col"></div>
                <div class="col-md-8">
                    <div class="answers" align="center">
                        
                        @if($level->level == 1)
                            <div class="row">
                                <div class="col q-1">
                                    <button class="" data-val="a" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                                <div class="col q-2">
                                    <button class="" data-val="b" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                            </div>
                        @endif
                        
                        @if($level->level == 2)
                            <div class="row">
                                <div class="col q-1">
                                    <button class="" data-val="a" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                                <div class="col q-2">
                                    <button class="" data-val="b" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col"></div>
                                <div class="col-6 q-3">
                                    <button class="" data-val="c" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                                <div class="col"></div>
                            </div>
                        @endif

                        @if($level->level == 3)
                            <div class="row">
                                <div class="col q-1">
                                    <button class="" data-val="a" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                                <div class="col q-2">
                                    <button class="" data-val="b" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col q-3">
                                    <button class="" data-val="c" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                                <div class="col q-4">
                                    <button class="" data-val="d" data-id="" data-src="" onclick="chosen(this)"><span>&#10003;</span></button>
                                    <h3></h3>
                                </div>
                            </div>
                        @endif
                        
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
        <div class="card-footer">
            <button onclick="next(this)" data-target="" class="btn btn-success btn-lg" disabled><b>NEXT</b></button>
        </div>
    </div>

    <div class="modal fade" id="completedModal" tabindex="-1" role="dialog" aria-labelledby="completedModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable|modal-dialog-centered modal-sm|modal-lg|modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="completedModalTitle" align="center">COMPLETED!</h5>
                </div>
                <div class="modal-body">
                    <img src="{{ asset('trophy.gif') }}" alt="">
                    <table class="table table-bordered table-inverse table-hover">
                        <tbody>
                            <tr>
                                <td>Correct Answers:</td>
                                <td class="correct_count"></td>
                            </tr>
                            <tr>
                                <td>Wrong Answers:</td>
                                <td class="wrong_count"></td>
                            </tr>
                            <tr>
                                <td>Number of Questions:</td>
                                <td class="question_count"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" align="center">
                    <a href="{{ route('level',$game->id) }}" type="button" class="btn btn-block btn-lg btn-success">Continue</a>
                </div>
            </div>
        </div>
    </div>
    <script>var questions = {!! $questions !!},game_id = {{ $level->game_id }}, level = {{ $level->id }}, answer = '', qid = '';</script>
    <script src="{{ asset('js/play.js') }}"></script>

        </div>
    </div>
</div>
</x-guest-layout>
