<x-guest-layout>
    <style>
        @media only screen and (max-width: 767px) {
            
        }
        .tab-content {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .nav-item {
            margin-right: 5px;
        }
        .nav-tabs .nav-link {
            background-color: #d7d7d7;
            font-weight: bold;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div style="margin-top: 50px;">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="scores-tab" data-toggle="tab" href="#scores" role="tab" aria-controls="scores" aria-selected="true">{{ ucwords(str_replace('_', ' ', $pupil->username)) }}'s progress</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="games-tab" data-toggle="tab" href="#games" role="tab" aria-controls="games" aria-selected="false">Select Game</a>
                        </li>
                    </ul>
                    <div class="tab-content bg-light" id="myTabContent">
                        <div class="tab-pane fade show active" id="scores" role="tabpanel" aria-labelledby="scores-tab">
                            
                            @if($logs->count())
                                
                                <table class="table table-responsive table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>Game / Level</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($logs as $log)
                                            <?php $q = json_decode($log->question->question); ?>
                                            <tr>
                                                <td>{{ $q->text }}</td>
                                                <td>{{ strtoupper($log->answer) }}</td>
                                                <td>{{ $log->game->name . ' / '. $log->level->name}}</td>
                                                <td>
                                                    @if($log->correct)
                                                    <span class="badge badge-pill badge-success">Correct</span>
                                                    @else
                                                    <span class="badge badge-pill badge-danger">Wrong</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                                {{ $logs->links() }}

                            @else

                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong>Oops!</strong> There are no scores to show at the moment.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                            @endif
                            
                        </div>
                        <div class="tab-pane fade" id="games" role="tabpanel" aria-labelledby="games-tab">
                            @if($games->count())
                                <?php
                                //Columns must be a factor of 12 (1,2,3,4,6,12)
                                $numOfCols = 2;
                                $rowCount = 0;
                                $bootstrapColWidth = 12 / $numOfCols;
                                ?>
                                <div class="row">
                                @foreach($games as $game)

                                    <div class="col-md-<?php echo $bootstrapColWidth; ?>" align="center">
                                        <div class="card bg-light border-light mb-4">
                                            <div class="card-header">
                                                <h1><b>{{ $game->name }}</b></h1>
                                            </div>
                                            <div class="card-body text-dark">
                                                @if( isset($images[$game->id]) && empty($images[$game->id]) == false )
                                                    <img src="{{ asset($images[$game->id]) }}" alt="">
                                                @endif
                                                <p>{{ $game->description }}</p>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="col">
                                                        <a href="{{ route('level',$game->id) }}" class="btn btn-lg btn-success btn-block">PLAY</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $rowCount++;
                                    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                                    ?>
                                @endforeach
                                </div>
                                
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
