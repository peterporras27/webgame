<x-guest-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="py-12">
                    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                            <div class="p-6 bg-white border-b border-gray-200">
                                <a href="{{ route('start') }}" class="btn btn-lg btn-success"> <i class="bi bi-arrow-left-circle-fill"></i> Select Game</a>
                                <p class="h1 text-center">{{ $game->name }}</p>
                                <br><br>
                                @if($levels->count())

                                    <?php
                                    //Columns must be a factor of 12 (1,2,3,4,6,12)
                                    $numOfCols = 3;
                                    $rowCount = 0;
                                    $bootstrapColWidth = 12 / $numOfCols;
                                    $count = 1;
                                    ?>
                                    <div class="row">
                                    @foreach($levels as $level)

                                        <div class="col-md-<?php echo $bootstrapColWidth; ?>" align="center">
                                            <div class="card bg-light border-light mb-4" style="max-width: 18rem;">
                                                <div class="card-header">
                                                    <h1 style="font-size: 10rem;"><b>{{ $count }}</b></h1>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="row">
                                                        <div class="col">
                                                            <a href="{{ route('play',[$level->id,$level->game_id]) }}" class="btn btn-lg btn-info btn-block">{{ $level->name }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $rowCount++;
                                        $count++;
                                        if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                                        ?>
                                    @endforeach
                                    </div>
                                @else

                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Holy guacamole!</strong> There are no levels available at the moment.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</x-guest-layout>
