<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Play') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if($games->count())
                        <?php
                        //Columns must be a factor of 12 (1,2,3,4,6,12)
                        $numOfCols = 3;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;
                        ?>
                        <div class="row">
                        @foreach($games as $game)

                            <div class="col-md-<?php echo $bootstrapColWidth; ?>" align="center">
                                <div class="card bg-light border-light mb-4" style="max-width: 18rem;">
                                    <div class="card-header">
                                        <b>{{ $game->name }}</b>
                                    </div>
                                    <div class="card-body text-dark">
                                        <p class="card-text">{{ $game->description }}</p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col">
                                                <a href="{{ route('games.edit',$game->id) }}" class="btn btn-lg btn-info btn-block">PLAY</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $rowCount++;
                            if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                            ?>
                        @endforeach
                        </div>
                        
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
