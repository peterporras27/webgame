<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Scores') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="h1 text-center">Score Board</p>
                    <br><br>
                    @if($pupils->count())
                        
                        <table class="table table-responsive table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Pupil</th>
                                    <th class="text-center">Points</th>
                                    <th class="text-right">Results</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pupils as $pupil)
                                    <tr>
                                        <td>{{ ucwords(str_replace('_', ' ', $pupil->username)) }}</td>
                                        <td class="text-center">{{ $pupil->score() }}</td>
                                        <td class="text-right">
                                            <a href="{{ route('pupils.show',$pupil->id) }}" class="btn btn-sm btn-info">View results</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $pupils->links() }}

                    @else

                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Oops!</strong> There are no scores to show at the moment.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
