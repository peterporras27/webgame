@props(['error'])

@if ($error)
    <div {{ $attributes->merge(['class' => 'alert alert-danger','role'=>'alert']) }}>
        {{ $error }}
    </div>
@endif
