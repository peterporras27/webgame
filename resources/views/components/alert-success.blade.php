@props(['success'])

@if ($success)
    <div {{ $attributes->merge(['class' => 'alert alert-success','role'=>'alert']) }}>
        {{ $success }}
    </div>
@endif
