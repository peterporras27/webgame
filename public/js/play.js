// var jQuery = jQuery;
jQuery(document).ready(function(){
	loadQuestion();
});

function loadQuestion(){

	if (questions.length > 0) {

		var q = JSON.parse(questions[0].question);
		var ans = JSON.parse(questions[0].answer);
		var a = JSON.parse(questions[0].a);
		var b = JSON.parse(questions[0].b);
		var c = JSON.parse(questions[0].c);
		var d = JSON.parse(questions[0].d);

		qid = questions[0].id;

		jQuery('.question img').attr('src','/img/'+q.image);
		jQuery('.question h3').html(q.text);

		var validFiles = ['mp3','wav'];

		if (level >= 1) {

			if (validFiles.includes(a.image.split('.').pop())) {
				jQuery('.q-1 button').attr('style','background-image: url(/play.png);height:100px;');
				jQuery('.q-1 button').attr('data-src', '/storage/'+a.image);
			} else {
				jQuery('.q-1 button').attr('style','background-image: url(/img/'+a.image+');');
			}

			jQuery('.q-1 button').attr('data-id',a.id);
			jQuery('.q-1 h3').html(a.text);

			if (validFiles.includes(b.image.split('.').pop())) {
				jQuery('.q-2 button').attr('style','background-image: url(/play.png);height:100px;');
				jQuery('.q-2 button').attr('data-src', '/storage/'+b.image);
			} else {
				jQuery('.q-2 button').attr('style','background-image: url(/img/'+b.image+');');
			}

			jQuery('.q-2 button').attr('data-id',b.id);
			jQuery('.q-2 h3').html(b.text);			
		}

		if (level >= 2) {

			if (validFiles.includes(c.image.split('.').pop())) {
				jQuery('.q-3 button').attr('style','background-image: url(/play.png);height:100px;');
				jQuery('.q-3 button').attr('data-src', '/storage/'+c.image);
			} else {
				jQuery('.q-3 button').attr('style','background-image: url(/img/'+c.image+');');
			}

			jQuery('.q-3 button').attr('data-id',c.id);
			jQuery('.q-3 h3').html(c.text);		
		}

		if (level >= 3) {

			if (validFiles.includes(d.image.split('.').pop())) {
				jQuery('.q-4 button').attr('style','background-image: url(/play.png);height:100px;');
				jQuery('.q-4 button').attr('data-src', '/storage/'+d.image);
			} else {
				jQuery('.q-4 button').attr('style','background-image: url(/img/'+d.image+');');
			}

			jQuery('.q-4 button').attr('data-id',d.id);
			jQuery('.q-4 h3').html(d.text);		
		}

	} else {

		jQuery('#completedModal').modal('show');

		jQuery.ajax({
			url: '/score/'+level+'/'+game_id,
			type: 'GET',
			dataType: 'json',
			data: {},
		})
		.always(function(e) {
			jQuery('.correct_count').html(e.correct_count);
			jQuery('.wrong_count').html(e.wrong_count);
			jQuery('.question_count').html(e.question_count);
		});

		// setTimeout(function(){
		// 	window.location.href = jQuery('.modal-footer a').attr('href');
		// },3000);
	}
}

function chosen(btn){

	jQuery('.answers button').removeClass('selected');
	jQuery('.answers button span').attr('style','');

	jQuery(btn).addClass('selected');
	jQuery(btn).find('span').attr('style','display:block;');

	answer = jQuery(btn).data('val');
	var audioLink = jQuery(btn).attr('data-src');

	if (audioLink != '') {
		var audio = new Audio(audioLink);
		audio.play();
	}

	jQuery('.card-footer button').prop('disabled',false);
}

function reset(){

	jQuery('.answers button').removeClass('selected');
	jQuery('.answers button span').attr('style','');
	jQuery('.card-footer button').prop('disabled',true);
}

function next(btn){

	var token = jQuery('[name="csrf-token"]').attr('content');

	jQuery.ajax({
		url: '/answer',
		type: 'POST',
		dataType: 'json',
		data: {question_id: qid, answer: answer, _token:token},
	})
	.always(function(e) {

		if (e.error == false) {

			reset();
			questions.shift();
			loadQuestion();
		}
	});
}