<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'password' => app('hash')->make('secret'),
            'role' => 'admin'
        ]);

        DB::table('users')->insert([
            'email' => 'user@user.com',
            'username' => 'pedro',
            'password' => app('hash')->make('secret'),
            'role' => 'user'
        ]);
    }
}
