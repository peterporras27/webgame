<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'name' => 'Color Game',
            'description' => 'Color game is a simple color identification game for students.'
        ]);

        DB::table('games')->insert([
            'name' => 'Number Game',
            'description' => 'Number game is a simple number counting game for students.'
        ]);

        DB::table('games')->insert([
            'name' => 'Sound Game',
            'description' => 'Sound game is a simple Sound identifying game for students.'
        ]);

        DB::table('levels')->insert(['name' => 'Level One', 'level' => 1, 'game_id' => 1]);
        DB::table('levels')->insert(['name' => 'Level Two', 'level' => 2, 'game_id' => 1]);
        DB::table('levels')->insert(['name' => 'Level Three', 'level' => 3, 'game_id' => 1]);

        DB::table('levels')->insert(['name' => 'Level One', 'level' => 1, 'game_id' => 2]);
        DB::table('levels')->insert(['name' => 'Level Two', 'level' => 2, 'game_id' => 2]);
        DB::table('levels')->insert(['name' => 'Level Three', 'level' => 3, 'game_id' => 2]);

        DB::table('levels')->insert(['name' => 'Level One', 'level' => 1, 'game_id' => 3]);
        DB::table('levels')->insert(['name' => 'Level Two', 'level' => 2, 'game_id' => 3]);
        DB::table('levels')->insert(['name' => 'Level Three', 'level' => 3, 'game_id' => 3]);
    }
}
