<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\GamesController;
use App\Http\Controllers\LevelsController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\PlayController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\PupilsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class,'index'])->name('home');
Route::get('img/{filename}', [PublicController::class,'img'])->name('img');
Route::get('dashboard', [PlayController::class,'index'])->name('dashboard');
Route::get('level/{id}', [PlayController::class,'level'])->name('level');
Route::get('play/{level_id}/{id}', [PlayController::class,'play'])->name('play');
Route::get('score/{level_id}/{id}', [PlayController::class,'score'])->name('score');
Route::post('answer', [PlayController::class,'answer'])->name('answer');
Route::get('scores', [LogsController::class,'index'])->name('scores');
Route::post('login/pupil', [PublicController::class,'pupil_login'])->name('pupil_login');

Route::get('play', [PublicController::class,'play'])->name('start');

require __DIR__.'/auth.php';

// Restful Controllers
Route::resources([
    'users' => UsersController::class,
    'games' => GamesController::class,
    'levels' => LevelsController::class,
    'questions' => QuestionsController::class,
    'pupils' => PupilsController::class
]);
